import os
from pathlib import Path
import json

def get_themes_files():
  home = str(Path.home())
  themes_path = home + '/.config/theme_colors/themit/themes/'

  files = os.listdir(themes_path)

  files.remove('__pycache__')
  files.remove('themes.py')
  files_ = list(map(lambda file: (file.replace('.json', ''), themes_path + file), files))

  return files_

def get_themes_names():
  files = get_themes_files()

  return list(map(lambda file_tuple: file_tuple[0], files))

def get_themes_palettes():
  files = get_themes_files()

  response = []
  for (file_name, file_path) in files:
    with open(file_path) as f:
      data = json.load(f)
      response.append((file_name, data))

  return response

def get_theme_palette(theme_name):
  home = str(Path.home())
  theme_path = home + '/.config/theme_colors/themit/themes/' + theme_name + '.json'

  response = {}
  with open(theme_path) as f:
    response = json.load(f)

  return response

