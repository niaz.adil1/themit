from helpers.colors import get_rgb_colors

RESET = '\033[0m'
def get_color_escape(col, background=False):
  r, g, b = col
  return '\033[{};2;{};{};{}m'.format(48 if background else 38, r, g, b)

def display (palette_name, palette):
  rgb_palette = get_rgb_colors(palette)

  palette_name = (palette_name + " "*22)[0:21]

  print('┌──\t', palette_name, '\t────────────────────────────────────────┐')

  print(
    '│', 'colors {0,7}  ',
    get_color_escape(rgb_palette['black_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['red_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['green_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['yellow_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['blue_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['magenta_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['cyan_dark'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['white_dark'], True) + '    '  + RESET
    + '                       │'
  )

  print(
    '│', 'colors {8,15} ',
    get_color_escape(rgb_palette['black_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['red_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['green_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['yellow_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['blue_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['magenta_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['cyan_light'], True) + '    '  + RESET
    + get_color_escape(rgb_palette['white_light'], True) + '    '  + RESET
    + '                       │'
  )

  print('└───────────────────────────────────────────────────────────────────────┘')

  print('\n')
