import subprocess as sp

from PIL import Image

def process_image(path, themit_path):
  set_wallpaper(path)
  resize_image(path, themit_path)

def set_wallpaper(path):
  command_set_background = f'feh --bg-scale {path}'
  command_create_copy = f'convert {path} ~/wallpaper.png'

  sp.getoutput(command_set_background)
  sp.getoutput(command_create_copy)

def resize_image(path, themit_path):
  after_path = themit_path + '__after.jpg'

  basewidth = 400
  img = Image.open(path)

  image_ext = (path.split('/')[-1]).split('.')[-1]

  if image_ext == 'png':
    img = img.convert('RGB')

  wpercent = (basewidth/float(img.size[0]))
  hsize = int((float(img.size[1])*float(wpercent)))
  img = img.resize((basewidth,hsize), Image.ANTIALIAS)
  img.save(after_path)

def remove_tmp_image(themit_path):
  after_path = themit_path + '__after.jpg'

  command = f'rm {after_path}'
  sp.getoutput(command)
