import os

def export_basic(colors, path):
  newdata = []
  for line in colors.items():
    (key, value) = line
    mod_key = "@@" + key
    mod_value = value.split("#")[-1]

    newdata.append(mod_key + "=" + mod_value)

  newdata_ = '\n'.join(newdata)

  f = open(path, 'w')
  f.write(newdata_)
  f.close()


def export_themes(templates_path, dst_path):
  response = get_files(templates_path, dst_path)

  base = response["base"]
  files = response["files"]

  base_file_data = extract_base_data(base)

  extract_themes(base_file_data, files)


def get_files(templates_path, dst_path):
  files = os.listdir(templates_path)

  files.remove('basic')
  base = templates_path + 'basic'
  files_ = list(map(lambda file: (templates_path + file, dst_path + file), files))

  return {
    "base": base,
    "files": files_,
  }


def extract_base_data(base):
  f = open(base,'r')
  base_file_data = f.readlines()
  f.close()

  colors=[]
  for line in base_file_data:
    line_ = line.replace('\n', '')
    name_col = line_.split('=')
    colors.append(name_col)

  return colors



def extract_themes(data, files):
  for fileset in files:
    (src, dst) = fileset

    f = open(src,'r')
    filedata = f.read()
    f.close()

    newdata = filedata
    for d_ in data:
      key = d_[0]
      value = d_[1]
      newdata = newdata.replace(key, value)

    f = open(dst,'w')
    f.write(newdata)
    f.close()
