import colorsys

# HSV TO HEX
def hsv_to_hex(color):
  return hls_to_hex(hsv_to_hls(color))

def hsv_to_hls(color):
  h, s, v = color
  l = v * (1 - s/2)
  s = 0 if l in (0, 1) else (v - l)/min(l, 1-l)
  return h, l, s

def hls_to_hex(color):
  return rgb_to_hex(hls_to_rgb(color))

def hls_to_rgb(color):
  h, l, s = color
  return list(map(lambda x: round(x * 255), colorsys.hls_to_rgb(h, l, s)))

def rgb_to_hex(color):
  return '#%02x%02x%02x' % tuple(color)

# HEX TO HSV
def hex_to_hsv(color):
  return hls_to_hsv(hex_to_hls(color))

def hex_to_hls(color):
  return rgb_to_hls(hex_to_rgb(color))

def hex_to_rgb(color):
  hexNum = color.strip('#')
  hexLen = len(hexNum)
  conversion = tuple(int(hexNum[i:i+hexLen//3], 16) for i in range(0, hexLen, hexLen//3))
  return conversion

def rgb_to_hls(color):
  r, g, b = map(lambda x: x/255.0, color)
  return colorsys.rgb_to_hls(r, g, b)

def hls_to_hsv(color):
  h, l, s = color
  v = l + s * min(l, 1-l)
  s = 0 if v == 0 else 2*(1 - l/v)
  return h, s, v
