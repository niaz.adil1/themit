import subprocess as sp

from helpers.convertors import hex_to_hsv, hsv_to_hex, hex_to_rgb

def get_colors(themit_path, limit = 1):
  after_path = themit_path + '__after.jpg'

  command = f'convert {after_path} +dither -colors {limit} -unique-colors txt:-'

  output = sp.getoutput(command)

  split_output = output.split('\n')[1:]

  return list(map(lambda x: hex_to_hsv(x.split(' ')[3]), split_output))

def adjust_color(color, ranges):
  h, s, v = color

  data = get_color_approx(color, ranges)

  hue_data = data['hue']
  saturation_data = data['saturation']
  value_data = data['value']

  _h = hue_data['correction'] if hue_data['correction'] else h
  _s = saturation_data['correction'] if saturation_data['correction'] else s
  _v = value_data['correction'] if value_data['correction'] else v

  return _h, _s, _v

def get_color_names(_colors, ranges):
  named_colors = {
    'red_dark': [],
    'red_light': [],
    'green_dark': [],
    'green_light': [],
    'yellow_dark': [],
    'yellow_light': [],
    'blue_dark': [],
    'blue_light': [],
    'cyan_dark': [],
    'cyan_light': [],
    'magenta_dark': [],
    'magenta_light': [],
  }

  for _col in _colors:
    data = get_color_approx(_col, ranges)
    hue_data = data['hue']
    value_data = data['value']

    name = hue_data['name'] + '_' + value_data['name']

    named_colors[name].append(_col)

  return named_colors

def get_color_approx(_color, ranges):
  h, s, v = _color

  _h = (h * 360) % 360

  hue_ranges = ranges['h']
  saturation_ranges = ranges['s']
  value_ranges = ranges['v']

  hue_name, hue_correction = hue_approx(_h, hue_ranges)

  saturation_correction = saturation_approx(s, saturation_ranges)

  value_name, value_correction = value_approx(v, value_ranges)

  data = {
    'hue': {
      'name': hue_name,
      'correction': hue_correction,
    },
    'saturation': {
      'correction': saturation_correction,
    },
    'value': {
      'name': value_name,
      'correction': value_correction,
    },
  }

  return data

def hue_approx(_h, hue_ranges):
  name = None
  correction = None

  range = 'wide'
  if _h >= hue_ranges['red']['wide'][0] or _h < hue_ranges['red'][('wide')][1]:
    name = 'red'
    if _h >= hue_ranges['red']['narrow'][0] or _h <= hue_ranges['red'][('narrow')][1]:
      range = 'narrow'
  else:
    for key, _ranges in hue_ranges.items():
      if _h >= _ranges['wide'][0] and _h < _ranges[('wide')][1]:
        name = key
        if _h >= _ranges['narrow'][0] and _h <= _ranges[('narrow')][1]:
          range = 'narrow'

  if range == 'wide':
    narrow_range = hue_ranges[name]['narrow']
    if _h < narrow_range[0]:
        correction = narrow_range[0] / 360
    if _h < narrow_range[1]:
        correction = narrow_range[1] / 360

  return name, correction

def saturation_approx(s, saturation_ranges):
  correction = None

  if s < saturation_ranges[0]:
    correction = saturation_ranges[0]
  if s > saturation_ranges[1]:
    correction = saturation_ranges[1]

  return correction

def value_approx(v, value_ranges):
  name = None
  correction = None

  if v <= value_ranges['dark'][1]:
    name = 'dark'
    if v < value_ranges['dark'][0]:
      correction = value_ranges['dark'][0]
  elif v > value_ranges['light'][0]:
    name = 'light'
    if v > value_ranges['light'][1]:
      correction = value_ranges['light'][1]

  return name, correction

def get_oppsosite_lightness_color(color, ranges):
  h, s, v = color

  name, correction = value_approx(v, ranges)

  v_ = v

  if name == 'dark':
    v_ = (ranges['light'][0] + ranges['light'][1]) / 2
  elif name == 'light':
    v_ = (ranges['dark'][0] + ranges['dark'][1]) / 2

  return h, s, v_

def get_hex_colors(_palette):
  hex_palette = {}
  for key, value in _palette.items():
    col = hsv_to_hex(value)
    hex_palette[key] = col
  return hex_palette

def get_rgb_colors(_palette):
  rgb_palette = {}
  for key, value in _palette.items():
    col = hex_to_rgb(value)
    rgb_palette[key] = col
  return rgb_palette

def create_background(color):
  h, s, v = color
  return h, 0.8, 0.1

def create_foreground(color):
  h, s, v = color
  return h, 0.6, 0.3
