import json
from pathlib import Path

from helpers.convertors import hex_to_hsv

def get_initial_options():
  home = str(Path.home())
  base_theme_path = home + '/.config/theme_colors/themit/themes/basic.json'

  _basic_palette = {}
  with open(base_theme_path) as f:
    _basic_palette = json.load(f)

  basic_palette = {}
  for key, value in _basic_palette.items():
    col = hex_to_hsv(value)
    basic_palette[key] = col

  h_ranges = {
    'red':{ 'wide': [330, 30], 'narrow': [355, 10] },
    'yellow':{ 'wide': [30, 60], 'narrow': [51, 60] },
    'green':{ 'wide': [60, 150], 'narrow': [81, 140] },
    'cyan':{ 'wide': [150, 210], 'narrow': [170, 200] },
    'blue':{ 'wide': [210, 270], 'narrow': [221, 240] },
    'magenta':{ 'wide': [270, 330], 'narrow': [281, 320] },
  }

  s_ranges = [0.2, 1.0]

  v_ranges = {
    'dark': [0.6, 0.8],
    'light': [0.8, 1.0],
  }

  ranges = {
    "h": h_ranges,
    "s": s_ranges,
    "v": v_ranges,
  }

  return basic_palette, ranges
