from helpers.colors import adjust_color, get_colors, get_color_names, get_oppsosite_lightness_color, get_hex_colors, create_background, create_foreground
from helpers.initialize import get_initial_options
from helpers.convertors import hsv_to_hex

def get_palette(themit_path):
  _palette, ranges = get_initial_options()

  value_ranges = ranges['v']

  main_color = get_colors(themit_path)[0]

  complimentary_color = get_complimentary_color(main_color)

  colors_list = []
  # MAIN_TRIADIC_COLORS
  main_color_ = get_colors(themit_path)[0]
  main_color = adjust_color(main_color_, ranges)
  colors_list.append(main_color)
  main_triadic_120, main_triadic_240 = get_triadic_colors(main_color)
  colors_list.append(main_triadic_120)
  colors_list.append(main_triadic_240)

  # COMPLEMENTARY_TRIADIC_COLORS
  complimentary_color = get_complimentary_color(main_color)
  colors_list.append(complimentary_color)
  complimentary_triadic_120, complimentary_triadic_240 = get_triadic_colors(complimentary_color)
  colors_list.append(complimentary_triadic_120)
  colors_list.append(complimentary_triadic_240)

  # OPPOSITE VALUE COLORS
  opposite_colors = list(map(lambda x: get_oppsosite_lightness_color(x, value_ranges), colors_list))

  _colors = colors_list + opposite_colors

  _named_colors = get_color_names(_colors, ranges)

  for key, cols in _named_colors.items():
    if len(cols) > 0:
      _palette[key] = cols[0]

  _palette['background'] = create_background(main_color)
  _palette['foreground'] = _palette['white_dark']

  hex_palette = get_hex_colors(_palette)

  return hex_palette, hsv_to_hex(create_foreground(main_color))

def get_complimentary_color(color):
  h, s, v = color

  deg_180_hue = ((h * 360.0 + 180.0) % 360) / 360.0

  return deg_180_hue, s, v

def get_triadic_colors(color):
  h, s, v = color

  deg_120_hue = ((h * 360.0 + 120.0) % 360) / 360.0
  deg_240_hue = ((h * 360.0 + 240.0) % 360) / 360.0

  color_120_rgb = deg_120_hue, s, v
  color_240_rgb = deg_240_hue, s, v

  return color_120_rgb, color_240_rgb
