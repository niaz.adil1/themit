from helpers.colors import adjust_color, get_colors, get_color_names, get_hex_colors, create_background, create_foreground
from helpers.initialize import get_initial_options
from helpers.convertors import hsv_to_hex

def get_palette(themit_path):
  _palette, ranges = get_initial_options()

  _s_colors = get_colors(themit_path, 1000)

  s_colors = list(map(lambda x: adjust_color(x, ranges), _s_colors))

  _named_colors = get_color_names(s_colors, ranges)

  for key, cols in _named_colors.items():
    if len(cols) > 0:
      _palette[key] = cols[0]

  _palette['background'] = create_background(s_colors[0])
  _palette['foreground'] = _palette['white_dark']

  hex_palette = get_hex_colors(_palette)

  return hex_palette, hsv_to_hex(create_foreground(s_colors[0]))
