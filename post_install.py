import subprocess as sp

def post_install():
  scripts = {
    'dotfiles': ['. $HOME/.config/dotfiles/_setup.sh']
  }

  for key, commands in scripts.items():
    for command in commands:
      sp.getoutput(command)
