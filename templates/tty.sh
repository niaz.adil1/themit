#!/bin/sh
[ "${TERM:-none}" = "linux" ] && \
    printf '%b' '\e]P0@@black_dark
                 \e]P1@@black_light
                 \e]P2@@red_dark
                 \e]P3@@red_light
                 \e]P4@@green_dark
                 \e]P5@@green_light
                 \e]P6@@yellow_dark
                 \e]P7@@yellow_light
                 \e]P8@@blue_dark
                 \e]P9@@blue_light
                 \e]PA@@magenta_dark
                 \e]PB@@magenta_light
                 \e]PC@@cyan_dark
                 \e]PD@@cyan_light
                 \e]PE@@white_dark
                 \e]PF@@white_light
                 \ec'