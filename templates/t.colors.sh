#!/bin/sh

background='#@@background'
foreground='#@@foreground'

color0='#@@black_dark'
color1='#@@black_light'
color2='#@@red_dark'
color3='#@@red_light'
color4='#@@green_dark'
color5='#@@green_light'
color6='#@@yellow_dark'
color7='#@@yellow_light'
color8='#@@blue_dark'
color9='#@@blue_light'
color10='#@@magenta_dark'
color11='#@@magenta_light'
color12='#@@cyan_dark'
color13='#@@cyan_light'
color14='#@@white_dark'
color15='#@@white_light'
