# themit
Inspired by Pywal

## Installation
```
mkdir ~/.config/theme_colors/themit
git clone https://gitlab.com/niaz.adil1/themit.git ~/.config/theme_colors/themit

```


Add the following to ~/.bashrc or ~/.zshrc
```
alias themit="python ~/.config/theme_colors/themit/main.py"

```

## Usage
```
themit /path/to/wallpaper
```

include the generated resources into the desired locations

~/.Xresources
```
#include "/home/user/.config/theme_colors/t.Xresources"
```
