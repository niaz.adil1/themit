from themes.themes import get_themes_palettes
from helpers.display import display

def display_themes():
  list = get_themes_palettes()

  for (file_name, data) in list:
    display(file_name, data)
