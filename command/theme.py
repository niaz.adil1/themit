from os.path import exists
from pathlib import Path

from themes.themes import get_theme_palette

from helpers.display import display
from helpers.convertors import hex_to_hsv, hsv_to_hex
from helpers.colors import create_foreground
from helpers.extract import export_basic, export_themes


def apply_theme(theme_name):
  home = str(Path.home())
  themit_path = home + '/.config/theme_colors/themit/'
  templates_path = themit_path + 'templates/'
  basic_template_path = templates_path + 'basic'
  dst_path = home + '/.config/theme_colors/'

  palette = get_theme_palette(theme_name)

  chosen_palette = palette
  lightmode_foreground = hsv_to_hex(create_foreground(hex_to_hsv(chosen_palette['background'])))

  appearence_mode = input("[d]ark mode (default) or [l]ight mode\n")
  if appearence_mode == 'l':
    chosen_palette['background'] = chosen_palette['white_light']
    chosen_palette['foreground'] = lightmode_foreground

  export_basic(chosen_palette, basic_template_path)
  export_themes(templates_path, dst_path)
