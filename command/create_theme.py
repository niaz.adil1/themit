from pathlib import Path

from methods._main import get_palette as main_get_palette
from methods._search import get_palette as search_get_palette

from helpers._image import process_image
from helpers.extract import export_basic, export_themes
from helpers.display import display
from helpers.initialize import get_initial_options
from helpers.colors import get_hex_colors

def create(wallpaper_path):
  _default_palette, ranges = get_initial_options()
  default_palette = get_hex_colors(_default_palette)

  home = str(Path.home())
  themit_path = home + '/.config/theme_colors/themit/'
  templates_path = themit_path + 'templates/'
  basic_template_path = templates_path + 'basic'
  dst_path = home + '/.config/theme_colors/'

  process_image(wallpaper_path, themit_path)

  palette_by_main, main_lightmode_foreground = main_get_palette(themit_path)
  palette_by_search, search_lightmode_foreground = search_get_palette(themit_path)

  palettes = {
    'palette_by_main': palette_by_main,
    'palette_by_search': palette_by_search,
    'default_palette': default_palette,
  }

  for key, value in palettes.items():
    display(key, value)

  palette_choice = input("Choose Theme: [1, 2, any for default]\n")

  chosen_palette = default_palette
  lightmode_foreground = default_palette['black_dark']
  if palette_choice == '1':
    chosen_palette = palette_by_main
    lightmode_foreground = main_lightmode_foreground
  elif palette_choice == '2':
    chosen_palette = palette_by_search
    lightmode_foreground = search_lightmode_foreground

  appearence_mode = input("[d]ark mode (default) or [l]ight mode\n")
  if appearence_mode == 'l':
    chosen_palette['background'] = chosen_palette['white_light']
    chosen_palette['foreground'] = lightmode_foreground

  export_basic(chosen_palette, basic_template_path)
  export_themes(templates_path, dst_path)
