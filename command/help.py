def display_help():
  print("\n \
\noptions: \
\n-h\t--help\t\tdisplays this manual \
\n-l\t--list\t\tlists all available themes \
\n-t\t--theme\t\tchoose a palette among the available themes \
\n-i\t--image\t\textract a palette from an image \
\n")
