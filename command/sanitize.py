from os.path import exists

from themes.themes import get_themes_names

def sanitize(args):
  number_of_args = len(args) - 1

  if number_of_args == 0:
    return no_arguments()

  elif number_of_args == 1:
    return one_argument(args[1])

  elif number_of_args == 2:
    return two_arguments(args[1], args[2])

  else:
    return None, None


def no_arguments():
  return "error", "Not enough arguments"


def one_argument(option):
  if option == '--list' or option == '-l':
    return "list", None

  if option == '--help' or option == '-h':
    return "help", None

  else:
    return "error", "Wrong arguments"


def two_arguments(option, value):
  if option == '--theme' or option == '-t':
    return theme_option(value)

  elif option == '--image' or option == '-i':
    return image_option(value)

  else:
    return "error", "Wrong arguments"


def theme_option(name):
  list = get_themes_names()

  if name in list:
    return "theme", name
  else:
    return "error", "Theme not found"

def image_option(file_path):
  file_exists = exists(file_path)

  if file_exists:
    return "image", file_path
  else:
    return "error", "Image not found"
