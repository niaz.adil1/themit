import sys
from os.path import exists
from pathlib import Path

from command.sanitize import sanitize
from command.help import display_help
from command.list import display_themes
from command.theme import apply_theme
from command.create_theme import create

from methods._main import get_palette as main_get_palette
from methods._search import get_palette as search_get_palette

from helpers._image import process_image, remove_tmp_image
from helpers.extract import export_basic, export_themes
from helpers.display import display
from helpers.initialize import get_initial_options
from helpers.colors import get_hex_colors

from post_install import post_install

def main():
  option, value = sanitize(sys.argv)

  if option == "help":
    display_help()

  elif option == "list":
    display_themes()

  elif option == "theme":
    apply_theme(value)
    post_install()

  elif option == "image":
    create(value)
    post_install()

  elif option == "error":
    print("error:", value)

  else:
    print("unknown error")

if __name__ == "__main__":
    main()
